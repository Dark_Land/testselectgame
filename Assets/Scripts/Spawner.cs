﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Spawner : MonoBehaviour
{
    [SerializeField] private LevelsGenerator _levelGenerator;
    [SerializeField] private GameCondition _gameCondition;
    [SerializeField] private Transform _startPoint;
    [SerializeField] private CardBundleData[] _dataSets;
    [SerializeField] private GameObject _cell;
    [SerializeField] private List<Cell> _currentCells;

    private CardBundleData _currentDataSet;
    private int _countCellsInRow = 3;
    private Vector3 _currentPositionInstantiateCell;

    public event System.Action<List<Cell>> OnSpawnedCells;

    private void Awake()
    {
        _currentDataSet = _dataSets[Random.Range(0, _dataSets.Length)];
        _currentDataSet = _dataSets[0];
        _levelGenerator.OnGenerateLevel += SpawnCells;
    }

    private void SpawnCells(int levelnumber)
    {
        DestroyPreviousLevelCells();
        if (!_gameCondition.IsGameOver)
        {
            _currentPositionInstantiateCell = _startPoint.position;
            int indexDataSet;
            for (int i = 0; i < (_countCellsInRow * levelnumber); i++)
            {
                indexDataSet = Random.Range(0, _currentDataSet.DataSetCell.Length);
                while(_currentDataSet.UsedCells[indexDataSet] == 1)
                {
                    indexDataSet = Random.Range(0, _currentDataSet.DataSetCell.Length);
                }
                _currentDataSet.SetUsedValue(indexDataSet);
                var cell = Instantiate(_cell, _currentPositionInstantiateCell, Quaternion.identity).GetComponent<Cell>();
                cell.InitGameCondition(_gameCondition);
                cell.SetSprite(_currentDataSet.DataSetCell[indexDataSet]);
                cell.CheckWrongSprite(_currentDataSet.WrongRotationCells);
                cell.SetValueCell(_currentDataSet.CellValue[indexDataSet]);
                cell.gameObject.AddComponent<BoxCollider2D>();
                _currentCells.Add(cell);
                _levelGenerator.InitCellAction(cell);
                if ((i != 0) && ((i + 1) % _countCellsInRow == 0))
                {
                    _currentPositionInstantiateCell.x = _startPoint.position.x;
                    _currentPositionInstantiateCell.y += _currentDataSet.DeltaCellPositionY;
                    continue;
                }
                _currentPositionInstantiateCell.x += _currentDataSet.DeltaCellPositionX;
            }
            OnSpawnedCells(_currentCells);
        }
        _currentDataSet = _dataSets[Random.Range(0, _dataSets.Length)];
    }

    private void DestroyPreviousLevelCells()
    {
        _currentDataSet.ResetToZeroUsedCells();
        foreach (Cell cell in _currentCells)
        {
            Destroy(cell.gameObject);
        }
        _currentCells.Clear();
    }
}
