﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LevelsGenerator : MonoBehaviour
{
    private int _levelNumber = 0;

    public int LevelNumber => _levelNumber;

    public event Action<int> OnGenerateLevel;

    public void InitCellAction(Cell cell)
    {
        cell.OnSelectCorrectCell += GenerateNextLevel;
    }

    private void Start()
    {
        GenerateNextLevel();
    }

    private void GenerateNextLevel()
    {
        _levelNumber++;
        OnGenerateLevel(_levelNumber);
    }
}
