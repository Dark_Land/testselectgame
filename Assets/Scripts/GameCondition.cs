﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class GameCondition : MonoBehaviour
{
    [SerializeField] private LevelsGenerator _levelGenerator;
    [SerializeField] private Spawner _spawner;
    [SerializeField] private GameObject _endGamePanel;

    private int _lastLevel = 3;

    private bool _isGameOver = false;

    public bool IsGameOver => _isGameOver;

    public string ValueToFind { get; private set; }
    public int LastLevel => _lastLevel;

    public System.Action<string> OnSetValueToFind;

    private void Awake()
    {
        _levelGenerator.OnGenerateLevel += CheckLastLevel;
        _spawner.OnSpawnedCells += SetValueToFind;
        ValueToFind = "";
    }

    private void ActivateEndGamePanel()
    {
        _endGamePanel.SetActive(true);
    }

    private void CheckLastLevel(int levelNumber)
    {
        if(levelNumber == (_lastLevel + 1))
        {
            _isGameOver = true;
            ActivateEndGamePanel();
        }
    }

    private void SetValueToFind(List<Cell> cells)
    {
        string randomValue = cells[Random.Range(0, cells.Count)].Value;
        while(randomValue == ValueToFind)
        {
            randomValue = cells[Random.Range(0, cells.Count)].Value;
        }
        ValueToFind = randomValue;
        OnSetValueToFind(ValueToFind);
    }
}
