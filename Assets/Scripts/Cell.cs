﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;

public class Cell : MonoBehaviour
{
    [SerializeField]private string _value;
    [SerializeField] private SpriteRenderer _cellSpriteRenderer;
    [SerializeField] private GameCondition _gameCondition;

    private int _normalZRotation = -90;
    private float _cellSize = 0.5f;
    private float _dotweenScaleTime = 1f;

    public string Value => _value;

    public Action OnSelectCorrectCell;

    private void Awake()
    {
        transform.DOScale(_cellSize, _dotweenScaleTime);
    }

    public void SetSprite(Sprite sprite)
    {
        _cellSpriteRenderer.sprite = sprite;
    }

    public void SetValueCell(string value)
    {
        _value = value;
    }

    public void InitGameCondition(GameCondition gameCondition)
    {
        _gameCondition = gameCondition;
    }

    public void CheckWrongSprite(Sprite[] wrongRotationCells)
    {
        foreach (Sprite sprite in wrongRotationCells)
        {
            if(_cellSpriteRenderer.sprite == sprite)
            {
                SetNormalZRotation();
                break;
            }
        }
    }

    private void SetNormalZRotation()
    {
        transform.rotation = Quaternion.Euler(0, 0, _normalZRotation);
    }

    private void OnMouseDown()
    {
        if(_gameCondition.ValueToFind == _value)
        {
            OnSelectCorrectCell();        
        }
    }
}
