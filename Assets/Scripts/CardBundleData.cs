﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New CardBundleData", menuName = "Card Bundle Data", order = 51)]
public class CardBundleData : ScriptableObject
{
    [SerializeField] private Sprite[] _dataSetCell;
    [SerializeField] private Sprite[] _wrongRotationCells;
    [SerializeField] private int[] _usedCells;
    [SerializeField] private string[] _cellValue;
    [SerializeField] private float _deltaCellPositionX;
    [SerializeField] private float _deltaCellPositionY;

    public float DeltaCellPositionX => _deltaCellPositionX;
    public float DeltaCellPositionY => _deltaCellPositionY;
    public Sprite[] DataSetCell => _dataSetCell;
    public Sprite[] WrongRotationCells => _wrongRotationCells;
    public int[] UsedCells => _usedCells;
    public string[] CellValue => _cellValue;

    public void SetUsedValue(int index)
    {
        _usedCells[index] = 1;
    }

    public void ResetToZeroUsedCells()
    {
        for(int i = 0; i < _usedCells.Length; i++)
        {
            _usedCells[i] = 0;
        }
    }
}

