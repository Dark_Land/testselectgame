﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiFindText : MonoBehaviour
{
    [SerializeField] private Text _text;
    [SerializeField] private GameCondition _gameCondition;

    private void Awake()
    {
        _gameCondition.OnSetValueToFind += SetText;
    }

    private void SetText(string value)
    {
        string str = "Find " + value;
        _text.text = str;
    }
}
